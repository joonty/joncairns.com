# My blog

It's been created with Jekyll. Feel free to poke around.

Social images courtesy of http://bostinno.streetwise.co/channels/social-media-share-icons-simple-modern-download/.

It builds automatically using a Bitbucket POST hook and a basic sinatra app, but for security reasons the S3 configuration has been left out.


## Requirements

- Java 8 JDK
- Imagemagick
