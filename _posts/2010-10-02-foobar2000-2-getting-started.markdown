---
author: joonty
comments: true
date: 2010-10-02 12:44:10+00:00
layout: post
slug: foobar2000-2-getting-started
title: 'foobar2000: 2. Getting started'
wordpress_id: 90
categories:
- foobar2000
tags:
- foobar2000
- tutorial
---

_This section goes through the basic functions of the music player, and introduces the concepts of components and interfaces._





## Introduction


So you've downloaded and installed foobar2000? When you run it for the first time, a quick appearance setup will prompt you to make some decisions:

[![foobar2000 quick set-up](http://blog.joncairns.com/wp-content/uploads/2011/09/quicksetupsmall.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/quicksetup.png)<span class="caption">The quick set-up box you will see when you first start foobar2000</span>

If you click once on a layout, the foobar window underneath will show you the shape of that layout. To start, it may be best just to choose "Simple Playlist + Tabs", then choose a colour scheme. You can always change the layout later.

What you see in front of you is called the "default user interface" (see image below). The user interface that you choose is essentially an aesthetic issue, and will not change how the music sounds, and will not affect your music library. The default UI displays your music in one big pile, and any playlists you create will, by default, be listed in tabs. Not particulary impressive is it? The first thing you need to do is add your music. You can do this by going to "File -> Preferences", then navigate to the "Media Library" tab, and adding the folder where your music is stored. Foobar will then run through your collection, adding as many files as it is currently compatible with.

[![The default layout for foobar2000](http://blog.joncairns.com/wp-content/uploads/2011/09/default_small.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/default.png)<span class="caption">The default layout</span>

Playing/pausing/stopping your music is intuitive, and a seekbar allows you to search the track. In the foobar menu, under "Playback", you can change the order of playback, with "shuffle", "random" and variants thereof.

And here comes important foobar related point number 1: components are absolutely essential to foobar, and you pick whichever ones you fancy. Comparable to a sweet shop pick-n-mix. See the next page for a run down on some of my favourite components.

But as a beginner to the wonder of foobar, I think that the most important thing to do is to change the interface, and this can be done by [downloading the ColumnsUI component](http://yuo.be/columns.php). Personally, I've never used the default UI, though I know some people do. ColumnsUI is more intuitive, and easier to understand for newcomers. In the ColumnsUI section of this tutorial I will go about explaining how to create an interface very similar to iTunes. However, before this I will go through some of the best components, then some of the features of foobar.

Here are two examples of foobar interfaces that I use.

[![My ColumnsUI layout](http://blog.joncairns.com/wp-content/uploads/2011/09/mycolumns_small.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/mycolumns.png)<span class="caption">My ColumnsUI layout</span>

[![My Panels layout](http://blog.joncairns.com/wp-content/uploads/2011/09/mypanels_small.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/mypanels.png)<span class="caption">My panels layout</span>

See how they're totally different? Such is the power of foobar...

**Next section**: [3. Components](http://blog.joncairns.com/2010/10/foobar2000-3-components/)
