---
author: joonty
comments: true
date: 2010-10-02 12:56:48+00:00
layout: post
slug: foobar2000-3-components
title: 'foobar2000: 3. Components'
wordpress_id: 172
categories:
- foobar2000
tags:
- foobar2000
- tutorial
---

## How to use them


Components are an integral and ingenious part of foobar2000. They come (normally) as a single dll file, and you put them in your "C:\Program Files\foobar2000\components" directory, or the components folder within your foobar directory. They provide a huge variety of things such as audio compatability (play all sorts of codecs), user interfaces, visualisations and playlist and music browsers. When you install foobar, you are only provided with the bare minimum of components, but there are hundreds of them around the internet. The vast majority have been created by clever and generous foobar fans with some raw programming knowledge, leaving us with the benefit.

You can see which components you have installed by navigating to "File -> Preferences", then clicking on components at the top.

For a list of major components go [here](http://wiki.hydrogenaudio.org/index.php?title=Foobar2000:Components) (also [here for the official list](http://www.foobar2000.org/components/index.html) and here for more [3rd party components](http://pelit.koillismaa.fi/plugins/)). Here are some that I consider essential:



	
  * **ColumnsUI (foo_ui_columns) - **The most intuitive user interface that exists, and the first thing you should download. Then head to my columnsUI guide to get going.

	
  * **Browser (foo_browser) - **One of my favourite components, giving an iTunes style organisation of your database with artist, album, genre or whatever you want. You can have as many browsers going as you want at any one time, and they are all self-contained in their own window.

	
  * **Quick Search (foo_uie_quicksearch) - **Allows you to search your entire music database for title, genre, album, artist - whatever you choose!

	
  * **Discogs (foo_discogs) - **Links to album information servers to find details for CDs, such as artist, album name and track information. Works very well, especially when combined with foo_freedb2.dll.

	
  * **Channel Spectrum (foo_uie_vis_channel_spectrum) - **A beautiful and (relative to its peers) low system impact spectrum meter. Ok, maybe it's not essential, but it is pretty cool.

	
  * **Pretty Popup (foo_prettypop) **- Provides a very customisable little box that pops up on your screen wherever you want, with track information and album artwork each time a new track is played.


And though not actually components, it's worth downloading the Ogg Vorbis ([http://www.vorbis.com/](http://www.vorbis.com/)) and FLAC ([http://flac.sourceforge.net/](http://flac.sourceforge.net/)) encoders, as they are both excellent codecs for your music.

You're ready to go! Move on to the features section to see what makes foobar the best music player around.

**Next: **[4. Features](http://blog.joncairns.com/2010/10/foobar2000-4-features/)
