---
author: joonty
comments: true
date: 2010-10-02 12:59:39+00:00
layout: post
slug: foobar2000-4-features
title: 'foobar2000: 4. Features'
wordpress_id: 175
categories:
- foobar2000
tags:
- foobar2000
- tutorial
---

There are so many things you can do with foobar. I can't possibly go through all of them here, but the important thing is to get you knowing enough about the program that you can discover things for yourself. Here I've just given the basics of mass-tagging, playlist organisation, replaygain and assigning keyboard shortcuts, but it is by no means exhaustive.



	
  * [4.1 Mass-tagging](http://blog.joncairns.com/2010/10/foobar2000-4-1-mass-tagging/)

	
    * Organise your music collection by doing bulk tags of your music

	
    * For those who's music is somewhat incomprehensively disorganised




	
  * [4.2 Playlist organisation](http://blog.joncairns.com/2010/10/foobar2000-4-2-playlist-organisation/)

	
    * How to create/delete/move/listen to/dance with playlists




	
  * [4.3 Replaygain](http://blog.joncairns.com/2010/10/foobar2000-4-3-replaygain/)

	
    * Bring your music collection up to the same volume level, non-destructively

	
    * Can be applied to individual tracks, albums or your entire collection




	
  * [4.4 Keyboard shortcuts](http://blog.joncairns.com/2010/10/foobar2000-4-4-keyboard-shortcuts/)

	
    * Assign keyboard shortcuts to anything, in context or global (i.e. if foobar isn't selected)

	
    * Assign to playback, playlist tools, visualisations, browser tools, etc.





