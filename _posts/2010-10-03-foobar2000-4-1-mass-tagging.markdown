---
author: joonty
comments: true
date: 2010-10-03 12:42:41+00:00
layout: post
slug: foobar2000-4-1-mass-tagging
title: 'foobar2000: 4.1 Mass-tagging'
wordpress_id: 179
categories:
- foobar2000
tags:
- foobar2000
- mass-tagging
- tutorial
---

## Introduction


Is your music messed up? Do some of your tracks have missing information? Fear not. Foobar can help you, with its mighty mass-tagging feature. Tagging will allow your foobar to provide track information such as title, artist, album, date, genre, etc. correctly without changing the filenames of your tracks. Mass-tagging is where tags are automatically applied to a large number of tracks at once.


## Properties page


There are several ways of mass-tagging. But first we need to look at the track properties window, and become familiar with it. If you right click on a track in your foobar main window then go to properties (or alternatively press Alt + ENTER with the track selected), you will get something like the following:

[![Metadata panel for a track](http://blog.joncairns.com/wp-content/uploads/2011/09/properties.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/properties.png)<span class="caption">Metadata panel for a track</span>

This is the metadata tab. What does this tell me? Well apart from the obvious things such as artist, title, etc., it also tells me its place within the album, how many tracks the album has and even what was used to encode the track (in my case, iTunes). The useful thing with these fields is that they can be called upon by foobar's script, so it's good to have as many fields filled as possible. It may be that your music collection is made up of tracks that have all sorts of missing fields, in which case you need to do some tagging.

If you navigate to the next tab, you will see a page like this:

[![Properties panel for a track](http://blog.joncairns.com/wp-content/uploads/2011/09/properties2.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/properties2.png)<span class="caption">Properties panel for a track</span>

This data is uneditable, and tells you things like the file location, the audio quality and size of the file.


## Tagging


Back to the metadata tab, you can change any of the information in the fields. Just click on the field and it will change to a little text editing box, and you can type the new tag in. Then click "apply", and your track information will change. And here's the key part: if you select more than one track and go to the metadata page, you can change a field and then apply it to all of these tracks.

For instance, if I have a whole load of Brian Wilson tracks, but some of them are called "brian Wilson", some "BRIAN WILSON", some "bRiAn WiLsOn" or whatever, I can change them all in one go to say "Brian Wilson". Nice and neat.

"Ah, but", I hear you say, "I'd like to have another field that, for instance, can tell me the artist's favourite colour".

Rrrrrriiiight. Well, you can do that. Go to "tools" then "add new field", type in a name for the field and a value for that track, and you're away.

A useful feature of foobar's columnsUI is that you don't have to go to the properties page to edit visible field data for individual tracks: just click on a track once, then again after a short time. The field will become an editable text box, and you can change the data there.

You can also automatically number your tracks under the properties page. If you select an album and head to the metadata page, then click "tools" and "Auto TrackNumber", it will fill in the values for you.


## Mass-tagging via freedb


Freedb is a component (see [components page](http://www.joncairns.com/tutorials/foobar/comp.php)) that allows you to connect to a database to collect tags for your music. It is very effective, and well-integrated into foobar. You can use it to determine tags for CDs that you want to rip into your collection, but also to get tags for albums that have been sitting in your collection for a while.

After installing the component to your "foobar2000/components" directory an option in the right-click menu will appear under "Tagging": "Get Tags from freedb". Try it out on an album that you have by selecting all of the tracks, and don't worry as it doesn't change anything automatically. After selecting the option from the menu, you will get something like this:

[![FreeDB tagger panel](http://blog.joncairns.com/wp-content/uploads/2011/09/freedb.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/freedb.png)<span class="caption">FreeDB tagger panel</span>

I selected my Roxy Music album and asked freedb to find some tags for it, and this it what it came up with. In the top left, where it says "matches", I can choose out of several sets of data that the database found. Freedb found all the track names for me, plus the artist, album name and genre. But wait: I like most of this match, such as the "1999 remastered edition" bit, but I don't like that weirdo genre "Proto-Punk". What the heck is "Proto-Punk"? But that's fine, as I can change any field on this page, then update all my tracks. If you deselect "Wipe out existing tags" at the bottom, your tracks will be appended with information that they didn't have before, rather than having the current information changed.

If you've found that this doesn't work, you could have selected some tracks that don't make up an entire album, so freedb can't work it out. Another possibility is that freedb doesn't have any information for your music, especially if it's very obscure. Check also that your firewall isn't blocking the transfer. And lastly, check in the preferences window, under "Tools -> Tagging", and see wahether there is a server listed. If there isn't, then click "add" and use the settings shown here:

[![FreeDB server panel](http://blog.joncairns.com/wp-content/uploads/2011/09/freedbserver.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/freedbserver.png)<span class="caption">FreeDB server panel</span>


## Doing it by hand


It may be that you have a lots of tracks that aren't in complete albums. If this is the case then you will have to use a combination of Google and common sense to apply the tags. If none of the field data is present, you can probably guess the title of the track from the filename, as it is usually hidden in there somewhere. Unfortunately, this is a slow process, but worth it in the end as foobar uses these tags for almost everything.

Next we'll look at how to organise your music into playlists.
