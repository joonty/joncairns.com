---
author: joonty
comments: true
date: 2010-10-03 12:50:13+00:00
layout: post
slug: foobar2000-4-2-playlist-organisation
title: 'foobar2000: 4.2 Playlist organisation'
wordpress_id: 192
categories:
- foobar2000
tags:
- foobar2000
- playlists
- tutorial
---

## Introduction


The way that playlists are shown in foobar depends on the UI that's being used, plus the component that you use to display the playlists. So I'll focus more on how playlists are created, but I'll be using ColumnsUI and Playlist Switcher to display my playlists here.


## What?


I'm not sure if there's any person who doesn't know what a playlist is, but I'll say anyway. It's a way of storing a group of tracks, so that you can return to it at any time and play just those tracks. For instance, if there's a particular genre you like to listen to, then rather than filtering your music for that genre each time you could just put it all in a playlist. I use playlists to group songs of a particular mood, such as "chill-out".


## Using album list to create playlists


The album list component comes with foobar2000, and shows a tree of your music collection, organised by a field of your choice. Here's my music organised by genre:

[![My media library](http://blog.joncairns.com/wp-content/uploads/2011/09/albumlist.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/albumlist.png)<span class="caption">My media library organised by genre</span>

You can choose to organise it by album, artist, etc. under "view".

Let's say I want to create an "ambient" playlist. In this case, I would right-click on "ambient" and go to "Send to New Playlist". The playlist created is automatically called "Ambient". You can also add new tracks to a playlist by choosing "Add to Current Playlist", and the playlist that is currently selected will be the target playlist.

You can also create a new playlist by going to "File -> New Playlist", or Ctrl + N. Whatever playlist style you are using, you can rename the playlist by right-clicking on it and selecting "Rename". Finally, you can save playlists to a file on your hard drive with "File -> Save Playlist" or by right-clicking on the playlist and "Save as...". This means you can back them up for safekeeping.


## Playlist utilities


For this to work, you need to download [playlist tools (foo_utils) from here](http://foosion.foobar2000.org/components/?id=utils&version=0.6.2+beta+6)

Using the playlist tools means that you can edit playlists from within the context menu. You will find playlist operations by right-clicking on a track or group of tracks and going to "Edit other". Here you get the following options:

[![Playlist management](http://blog.joncairns.com/wp-content/uploads/2011/09/playlists.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/playlists.png)<span class="caption">Playlist management context menu</span>

"Add to", "Insert into" and "Send to" are all slightly different. The most important difference is with "Send to", which will replace everything in your playlist with whatever you are sending. The others will append your playlist. The subtle difference is that "add to" will add the new songs in at the end of the playlist, whereas "insert into" will place them just after a selected track in the playlist.

You should now know enough to create playlists quickly and efficiently, enjoy! Next we'll go on to the replaygain feature to normalise the volume of your tracks.

**Next section: **[4.3 Replaygain](http://blog.joncairns.com/2010/10/foobar2000-4-3-replaygain/)
