---
author: joonty
comments: true
date: 2010-10-03 12:54:54+00:00
layout: post
slug: foobar2000-4-3-replaygain
title: 'foobar2000: 4.3 Replaygain'
wordpress_id: 195
categories:
- foobar2000
tags:
- foobar2000
- replaygain
- tutorial
---

_Replaygain is used to normalise sound levels across your music collection. This section shows you how._

If some of your albums or tracks are at a different volume level to others, you may consider using the replaygain feature. I won't spend long on this, simply because I don't find that I need to use it. First, go to "Preferences -> Playback", and you'll see a page like the following:

[![Replaygain panel](http://blog.joncairns.com/wp-content/uploads/2011/09/replaygainsmall.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/replaygain.png)<span class="caption">Replaygain panel</span>

The replaygain section in the middle is what you are interested in. Source mode will determine whether you want the gain to be applied to albums or individual tracks, or not at all. My music collection is made up almost entirely of albums, so I keep it here as I find that albums fluctuate in volume from one another rather than within themselves. The preamp is a way of reducing or increasing the volume of tracks, and can be applied to those with or without replaygain info (I'll explain that in a second). I tend to put the preamp for "Without RG info" on about -6.00dB, as that seems to even things out.

To apply replay gain to tracks or albums, right click and go to "ReplayGain". You'll get a menu like this:

[![Replaygain context menu](http://blog.joncairns.com/wp-content/uploads/2011/09/replaygain2.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/replaygain2.png)<span class="caption">Replaygain context menu</span>

"Scan Per-File Track Gain" will provide RG info for individual tracks, and will give you information such as average level in dB, peak level, etc. "Scan Selection As Single Album" will take you selection to be a single album, and then work out the variation in level between the tracks. "Scan Selection As Albums (by tags)" will compare multiple albums with eachother, as long as the tags are correct. I find the last to be most useful, as it will correct gain in whole albums.

Selecting any of these will cause foobar to run through the tracks quickly and create some information. It will then save this information for the track or album. If you decide that you no longer want the information, just choose "Remove ReplayGain Info From Files". And you should never need to edit the info, which is the last option.

With the settings applied correctly in the preferences, you should find that foobar will interpret the RG info and correct your volume levels automatically.

Next I'll go through applying keyboard shortcuts to anything you fancy.

**Next section: **[4.4 Keyboard shortcuts](http://blog.joncairns.com/2010/10/foobar2000-4-4-keyboard-shortcuts/)
