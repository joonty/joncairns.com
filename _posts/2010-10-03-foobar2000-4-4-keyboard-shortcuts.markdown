---
author: joonty
comments: true
date: 2010-10-03 13:02:50+00:00
layout: post
slug: foobar2000-4-4-keyboard-shortcuts
title: 'foobar2000: 4.4 Keyboard shortcuts'
wordpress_id: 198
categories:
- foobar2000
tags:
- foobar2000
- tutorial
---

_In this part of the features section, keyboard shortcuts will be explained. You can create a keyboard shortcut for just about everything in foobar2000, making it a very powerful tool._


## Introduction


One of my favourite features of foobar is the ability to map shortcuts to practically any function in foobar, and being able to set them as global shortcuts. This way, you can control playback whilst browsing the internet, for example, and you don't need to navigate to your foobar to do it. The shortcuts page takes a little bit of explaining, but is quite simple in the end.


## Navigation


The keyboard shortcuts preferences, like everything else, can be found on the prefences page: it's under "General". When you open the page, you'll find something like this:

[![Keyboard shortcuts](http://blog.joncairns.com/wp-content/uploads/2011/09/keyboardsmall.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/keyboard.png)<span class="caption">Keyboard shortcuts preference panel</span>

There will already be some assigned shortcuts, and it's worth looking for any that you will find useful, and remembering the shortcut.

The "Key" and "Modifiers" columns show the shortcut combination, and though it's not necessary to have a modifier like Ctrl, Alt or Shift, it stops interference with most keys if you do. The F keys are pretty much up for grabs, however. "Global" shows whether the shortcut can be used anywhere or not. "Type" indicates the group which the action comes under, and then "Action" shows the result of using the shortcut.


## Adding a shortcut


As an example, I'm going to add my favourite shortcut, a global shortcut to play or pause. I'm going to choose Ctrl+Shift+Space as my shortcut, which won't interfere with anything (it's important to consider this, especially with global shortcuts), and is easy to use with just one hand. When you click "Add New", you'll get something like this:

[![New keyboard shortcut](http://blog.joncairns.com/wp-content/uploads/2011/09/newshortcut.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/newshortcut.png)<span class="caption">New keyboard shortcut</span>

In the action box underneath you will find "Play or Pause" under the "Main: Playback:" group. Alternatively, you can filter the list by typing in "play" in the filter list. Select "Play or Pause", then click on the "Key" box and input the shortcut you would like. Then select "Global Hotkey" to make it useable everywhere. Then click "Save All", and you're away.

Have a browse through the list of actions (though there are hundreds!) or try and think which actions you perform a lot, so you can make the process much quicker.

The next section looks at an introduction to using the interface ColumnsUI, and setting up your first skin.

**Next section: **5. ColumnsUI
