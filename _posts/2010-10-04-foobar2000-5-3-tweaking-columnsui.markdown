---
author: joonty
comments: true
date: 2010-10-04 13:12:25+00:00
layout: post
slug: foobar2000-5-3-tweaking-columnsui
title: 'foobar2000: 5.3 Tweaking ColumnsUI'
wordpress_id: 225
categories:
- foobar2000
tags:
- columnsUI
- foobar2000
- tutorial
---

_This section involves the use of in-program code to make tweaks to the appearance and functionality of your foobar._

To do this section you will need to be familiar with foobar's code syntax. If you aren't then take a look at part 6 first, then come back here.


## Editing the title bar


We'll start off nice and easy. Under the "main" tab of ColumnUI's preferences, at the bottom of the window, you'll find a box entitled "Main window title format script". You can freely edit what it says at the top of your foobar using titleformatting, instead of having just the default "foobar2000". I like to have it display the song title, artist name and the playback time and length, so the script will look like this:

{% highlight php %}
foobar 2000 - %artist%: %title% '('%playback_time%/%length%')'
{% endhighlight %}

This gives a title bar that looks like this:

[![Window title bar](http://blog.joncairns.com/wp-content/uploads/2011/09/titlebar.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/titlebar.png)<span class="caption">Window title bar</span>

The title bar format area will read everything you put in, including spaces, and doesn't require quotes for words and most letters. Certain letters such as $ and ( or ) need to be enclosed in apostrophes to show.

At one point I made a little progress bar that appeared in the title bar, and though it can look a bit naff the code would be something like this:

{% highlight php %}
|$progress(%_time_elapsed_seconds%,%_time_total_seconds%,100,#,-)|
{% endhighlight %}

[![Window title progress bar](http://blog.joncairns.com/wp-content/uploads/2011/09/progress.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/progress.png)<span class="caption">Window title progress bar</span>

Lovely, no? You can change the length of the bar by changing "100" to whatever number you like, and can change the characters that make up the bar.

Would love to hear if you come up with anything inventive and attractive to put in the title bar!


## Global variables


An important concept in ColumnsUI is the global variable. This works by setting a variable that can be called upon anywhere within the columnsUI appearance preferences. They are set in the globals window, under "playlist view", and in the variables tab:

[![Playlist view global variables](http://blog.joncairns.com/wp-content/uploads/2011/09/globals2small.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/globals2.png)<span class="caption">Playlist view global variables</span>

Global variables follow the same structure as normal variables, i.e.:

{% highlight php %}
$set_global(myvariable,something)
{% endhighlight %}

In this case, "something" is put into "myvariable". They can then be called by using:

{% highlight php %}
$get_global(myvariable)
{% endhighlight %}

This is very useful for defining a set of colours that you will use regularly, particularly when it comes to using the "Columns" tab (top of the window, I'll go through that soon). For instance,

{% highlight php %}
$set_global(mycolour1,$rgb(233,0,20))
{% endhighlight %}

sets a reddish colour as a global variable "mycolour1" that I can call upon any time.


## Columns editing


This is where columnsUI gets really good. You see all those columns in the columns playlist on your foobar? You can edit all sorts of things about them, such as what they display, their visual style and the way they sort (though I won't touch on that, I think they sort fine). On the "playlist view" page in preferences head to the "columns" tab at the top of the window, and you'll see something like this:

[![Columns in playlist view](http://blog.joncairns.com/wp-content/uploads/2011/09/columnsviewsmall.png)](http://blog.joncairns.com/wp-content/uploads/2011/09/columnsview.png)<span class="caption">Columns in playlist view</span>

Yours, however, will look different as I have a silly number of columns, for different occasions.

_More coming soon...hopefully. Although I wrote this about 3 years ago._

**Next section: **[6. Code Syntax](http://blog.joncairns.com/2010/10/foobar2000-6-code-syntax/)
