---
author: joonty
comments: true
date: 2010-10-04 12:46:55+00:00
layout: post
slug: foobar2000-5-columnsui
title: 'foobar2000: 5. ColumnsUI'
wordpress_id: 214
categories:
- foobar2000
tags:
- columnsUI
- foobar2000
- tutorial
---

This guide is broken into three parts:



	
  * [5.1 Finding your way around ColumnsUI](http://blog.joncairns.com/2010/10/foobar2000-5-1-getting-started-with-columnsui/)

	
    * Provides a basic tutorial on using ColumnsUI for the first time, and navigating the columnsUI preferences

	
    * Goes through the process of creating a simple layout




	
  * [5.2 Creating an iTunes style layout](http://blog.joncairns.com/2010/10/foobar2000-5-2-creating-a-columnsui-layout/)

	
    * Both a simple and advanced aproach to creating an iTunes style foobar

	
    * A look at fonts and colours

	
    * Short tutorial on using foo_browser

	
    * Image and code downloads for creating the perfect lookalike




	
  * [5.3 Code-based tweaks (advanced)](http://blog.joncairns.com/2010/10/foobar2000-5-3-tweaking-columnsui/)

	
    * Some more loose ends with columnsUI

	
    * Editing your toolbar

	
    * Changing playlist properties

	
    * Using frames, progress bars, etc.





The third section involves the use of foobar's scripting language, which is explained more in the next section. If you're new to this, it's advised that you miss it out, and maybe come back to it after looking at the code section.
