---
author: joonty
comments: true
date: 2010-10-05 12:27:22+00:00
layout: post
slug: foobar2000-6-code-syntax
title: 'foobar2000: 6. Code syntax'
wordpress_id: 260
categories:
- foobar2000
tags:
- code
- foobar2000
- tutorial
---

This section goes through the syntax of foobar's in-program code. It will allow you to pick up the fundamentals, and hopefully start writing code of your own.





## Know your friends


Before you do anything else, you need to bookmark this site: [http://wiki.hydrogenaudio.org/index.php?title=Foo...](http://wiki.hydrogenaudio.org/index.php?title=Foobar2000:Titleformat_Reference).

There is no chance that you can remember all of the hundreds of functions and field references, and there is no shame in having them all in one handy place. There is also a similar page for panelsUI-specific commands: [http://wiki.hydrogenaudio.org/index.php?title=Foob...](http://wiki.hydrogenaudio.org/index.php?title=Foobar2000:Components_0.9/Panels_UI_%28foo_ui_panels%29/Commands), but there will be more on panelsUI code later.


## Syntax introduction


Foobar's code is known as "title formatting". The declarations can be divided into two main categories: field references and functions. Field references are things which you can call upon to return a value, such as the name of the artist of the current track or the length of a song. Functions will perform a particular action on something, such as converting a word to uppercase letters or defining a colour.

Field references are within two percentage symbols, e.g. `%artist%`, which would return the name of the artist. You can find and edit some of these if you go to the properties page of a track, such as `%title%, %album%, %genre%`, etc.

Functions are written with a dollar symbol beforehand, and brackets enclosing the functionee, e.g. `$upper(%artist%)`, which would return the name of the artist but in uppercase letters. You can have functions within functions, and field references within functions are essential to editing.

Strings are also an important concept. They simply mean words, i.e. a string of letters, so, for example, `%artist%` returns a string.

Sound simple? Well it is, at the moment. This is just the tip of the iceberg, Timmy.

One quick note: I won't really be providing any sort of context for the code on this page. This will be about getting you familiar with the way the code works, and I'll put it in context on the respective columnsUI and panelsUI pages.


## If statements


If you are familiar with any programming language then you will know about _if statements_. I'll explain them anyway. They are extremely instrumental in all languages, and allow for programs, or foobars in this case, to make intelligent decisions. They work by saying "if this is true then I'll do this, but if it isn't true I'll do this instead".

Let's take the following example: I want one of my columns in my foobar to display the artist name, but if there is no artist then I want it to say "No Artist" rather than just saying nothing. Foobar's if statement looks like the folloing:

{% highlight php %}
$if( condition, thendothis, otherwisedothis )
{% endhighlight %}

The statement will always treat the "condition" as true or false, 1 or 0, so it needs to be the right kind of variable. So I could put `%artist%` in the condition part and the evaluate part, and that would be fine. My code will look like this:

{% highlight php %}
$if(%artist%,%artist%,'No Artist')
{% endhighlight %}

If you are using the default UI or columnsUI, you will have to put words that are in functions (like above, with 'No Artist') within single quotes. PanelsUI allows for no quotes.

There are several variants on the if statement with foobar, such as:

{% highlight php %}
$ifgreater(a,b,one,two)
{% endhighlight %}

_If a is greater than b then carry out one, otherwise two_

{% highlight php %}
$if3(a,b,c,d... else)
{% endhighlight %}

_This will keep evaluating any arguments until one is found to be true, and then it returns that value; otherwise returns else_

The other `$if` related statements can be found on the [title formatting](http://wiki.hydrogenaudio.org/index.php?title=Foobar2000:Titleformat_Reference#.24if2.28a.2Celse.29) web page.


## Variables


Foobar allows for the use of variables, a basic building block of programming. I won't spend time explaining what a variable is, but I'll give an example. A variable is defined by using the `$put` or `$puts` statement, which does what it says: it "puts" something (a word, number, field reference, etc.) into storage for later use. You can then recover this variable with the `$get` statement. The `$put` or `$puts` statement works like this:

{% highlight php %}
$put(myvariable,something)
{% endhighlight %}

"something" will then be stored in a variable called "myvariable". To return the variable at any time, we would use `$get(myvariable)`. This would give the output of "something". E.g.:

{% highlight php %}
$put(doo,foo)
$put(dah,bar)
get(doo)$get(dah) = foobar
{% endhighlight %}

`$put` and `$puts` work differently in that `$put` will always output whatever you give it as well as storing it, whereas `$puts` will only store it.


## Colours and pretty things


You can define a colour for text using `$rgb(r,g,b,r1,g1,b1)`, where _r_, _g_ and _b_ define the normal colour in red, green and blue and _r1_, _g1_ and _b1_ define the colour when the text is selected. Useful for specific visual styling. For other methods of defining colours see the title formatting page.

Colours can also be blended using the `$blend` function:

{% highlight php %}
$blend(colour1,colour2,x,total)
{% endhighlight %}

where x is however many "parts" of colour1 and total is the total number of "parts". Therefore the number of parts of colour2 will be total-x. The result will be a mixture of the two colours, much like mixing a drink. Nine parts gin, one part tonic...

Strings can have a colour transition applied to them, using the `$transition` function:

{% highlight php %}
$transition(string,color1,color2)
{% endhighlight %}

As a bit of an unattractive example, choosing `%artist%` as the string and the colours as red and blue, you would get something like the following:


![Example colour transition](http://blog.joncairns.com/wp-content/uploads/2011/09/transition.png)


Unfortunately, it's hard to make that function look good. So go for subtle transitions if you are going to use it.


## String functions


There are loads. You can do almost anything with your words, if you know how - this is another great strength of foobar. So check out the title formatting page (I can't be bothered to keep putting a link on that, just look at the top) and scroll to the strings bit. Here are some of Jon's top string functions:



	
  * `$upper(s)` - converts string s to uppercase

	
  * `$len(s)` - calculates length of string

	
  * `$char(x)` - inserts [unicode character (list here)](http://en.wikipedia.org/wiki/List_of_Unicode_characters), but you need it in decimal mode, so use [this site](http://rishida.net/scripts/uniview/conversion.php)

	
  * `$progress(pos,range,len,a,b)` - creates progress bar where _pos_ would be `%_time_elapsed_seconds%`, _range _is `%_time_total_seconds%`, _len_ is how long you want the bar to be, and _a_ and _b_ are characters/strings that will make the physical appearance of the bar


There are plenty more, such as functions that cut strings up into pieces, compare two strings to each other and move prefixes such as "A" and "The" to the end of the string.


## Mathematical/boolean functions


If it really gets you going, you might like to do a bit of maths while listening to your music. And I'm happy to report that foobar can accomodate you, you weirdo.



	
  * `$add(a,b)`, `$mul(a,b)` - adds/multiplies a and b

	
  * `$div(a,b)`, `$sub(a,b)` - divides a by b, subtracts b from a

	
  * `$greater(a,b)` - if _a_ is greater than _b_ returns true (i.e. "1"), otherwise false


Etc. The others are listed... guess where?

Boolean functions such as **AND**, **OR**, **NOT** are also supported by foobar.


## Final words


Well that's it my friends. You will soon enjoy a total dive into social isolation as you realise how stupidly addictive it is just to tweak even the tiniest settings of your foobar.

Enjoy.

**Next section:** [7. PanelsUI](http://blog.joncairns.com/2010/10/foobar2000-7-panelsui/)
