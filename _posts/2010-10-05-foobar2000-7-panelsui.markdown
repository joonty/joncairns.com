---
author: joonty
comments: true
date: 2010-10-05 12:55:59+00:00
layout: post
slug: foobar2000-7-panelsui
title: 'foobar2000: 7. PanelsUI'
wordpress_id: 257
categories:
- foobar2000
tags:
- foobar2000
- panelsUI
- tutorial
---

This guide is broken into three parts:



	
  * [7.1 Finding your way around PanelsUI](http://blog.joncairns.com/2010/10/foobar2000-7-1-finding-your-way-around-panelsui/)

	
    * Provides a basic tutorial on using PanelsUI for the first time, and navigating the various sectionsof the interface

	
    * Very basic appearance tweaks will be covered




	
  * [7.2 PanelsUI code](http://blog.joncairns.com/2010/10/foobar2000-7-2-panelsui-code/)

	
    * The code syntax for panelsUI will be explained

	
    * The differences between standard title formatting code (see [previous page](http://blog.joncairns.com/2010/10/foobar2000-6-code-syntax/)) and panelsUI code will be highlighted

	
    * Particular focus rectangle, image and button functions

	
    * Introduction to persistent variables (PVARs)




	
  * [7.3 Creating a layout](http://blog.joncairns.com/2010/10/foobar2000-7-3-creating-a-panelsui-layout/)

	
    * Creating your first layout with panelsUI and some basic graphics editing





A lot of this tutorial is fairly advanced, and if you are new to foobar2000, it is recommended that you start at the [beginning!](http://blog.joncairns.com/2010/10/1-what-is-foobar2000/) Otherwise, onwards...
