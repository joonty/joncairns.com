---
author: joonty
comments: true
date: 2011-09-22 13:14:19+00:00
layout: post
slug: jtools-google-chrome-plugin
title: jTools Google Chrome plugin
wordpress_id: 47
categories:
- Projects
tags:
- Github
- Google Chrome
- JavaScript
- jTools
---

One of the tools I use on a daily basis when browsing the Internet is the developer tools that come with webkit based browsers (i.e. Google Chrome and Safari). The "Elements" tab gives you a slick breakdown of the entire page DOM, and allows for easy manipulation.

At some point that I can no longer remember, I thought it would be useful to have a similar thing, but using [jQuery](http://www.jquery.com) selectors instead. For instance, if you want to find out the jQuery selector representation of a particular element of a page, how would you do it? What if you wanted to test a selector on a particular website, and see what it would return?


## Enter jTools


jTools is a plugin for Google Chrome that uses jQuery to provide various methods of page analysis on the site you're currently on, in real time. It currently has two modes:



	
  1. Execute a jQuery selector and highlight the objects on the page that match

	
  2. Hover over elements on a page and see the jQuery selector that represents it


It's still very much in development at the moment, so I haven't packed it up as a proper Chrome extension. In fact, the only access to it at the moment is through [Github](https://github.com/joonty/jtools). Here's how to give it a go.


### 1. Grab the Code


You need git installed on your machine - [follow these instructions](http://help.github.com/set-up-git-redirect) if you haven't got it yet. Then run:

{% highlight bash %}
$ git clone git@github.com:joonty/jtools.git
{% endhighlight %}

That will grab all the source code.


### 2. Load in Chrome


Click the spanner (hand wrench for Americans) at the top right of Google Chrome, then go to Tools -> Extensions. Hopefully you'll see a button saying "Load Unpacked Extension" near the top of the page. If not, click a link saying "Developer Mode" at the right of the page and it should appear. In the file browser, navigate to the directory where you just cloned the source and accept.

This should load the extension into your browser.


### 3. Try it Out


Reload any of your open tabs to try the extension out. You will see a jQuery logo in the top right of the screen, for inserting jQuery selectors into the current page, and a logo at the bottom right of each page, for analysing the DOM jQuery style.

I might add more information here as the project grows, but I'm just going to leave you to mess around with it for the time being.


### 4. Collaborate


I'm all for social coding, so for the technical amongst you please feel free to add to the project and work with me on my Git repository.
