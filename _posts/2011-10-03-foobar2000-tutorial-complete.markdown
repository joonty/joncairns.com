---
author: joonty
comments: true
date: 2011-10-03 20:54:54+00:00
layout: post
slug: foobar2000-tutorial-complete
title: foobar2000 tutorial complete
wordpress_id: 296
categories:
- foobar2000
post_format:
- Status
tags:
- foobar2000
- update
---

Just a quick update to say that the foobar2000 tutorial is back up again, and has a whole new set of links. Please let me know if you find any broken links or problems in general - I haven't been through it with a fine tooth comb yet.
