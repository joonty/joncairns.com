---
author: joonty
comments: true
date: 2011-10-31 22:46:01+00:00
layout: post
slug: i-am-in-fact-alive
title: I am in fact alive
wordpress_id: 327
categories:
- General
post_format:
- Status
tags:
- git
- update
---

I am, in fact, alive. Mere busyness seems like a luxury at the moment; I feel like I have achieved whatever is the next level above. However, I have found time (an overwhelmingly extravagant 5 minutes) to update my git submodule tutorial. Which is about updating git submodules. Some sort of recursive madness, it seems.
