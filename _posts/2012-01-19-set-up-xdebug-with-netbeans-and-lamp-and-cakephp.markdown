---
author: joonty
comments: true
date: 2012-01-19 13:42:04+00:00
layout: post
slug: set-up-xdebug-with-netbeans-and-lamp-and-cakephp
title: Set up Xdebug with Netbeans and LAMP (and CakePHP)
wordpress_id: 416
categories:
- Dev
tags:
- LAMP
- linux
- netbeans
- xdebug
---

If you're a regular user of [Netbeans IDE](http://netbeans.org/) then, like me, you might sometimes get the feeling that you're not using it to its full potential. Take debugging for instance: did you know that you can use Netbeans to debug a web application by pausing at particular lines of code, and get a list of all the variables in the current scope? It takes a bit of setting up, but you can do it - here's how.

_I've written this tutorial using Ubuntu 11.10 with Netbeans IDE version 7. It's recommended that you have this version of Netbeans, as certain things change pretty drastically from version to version. This tutorial mentions CakePHP specific set-up from time to time, but will work for non-Cake projects. I assume that you have a working LAMP environment (Linux, Apache, MySQL and PHP) with PHP 5+. If not, Google it ^_^_


## 1. Install Xdebug


If you're using Ubuntu, this is as gloriously easy as typing this in a terminal:

{% highlight bash %}
$ sudo apt-get install php5-xdebug
{% endhighlight %}

If you're not, don't worry - you can install it using `pecl`. (Having said that, I've frequently found that `pecl` doesn't work the way it's supposed to, and have had to compile extensions manually. Drop a comment if you have problems.)

{% highlight bash %}
$ sudo pecl install xdebug
{% endhighlight %}

Providing that one of the two previous commands worked, you need to add some configuration settings so that PHP can use Xdebug. Edit or create the file `**/etc/php5/conf.d/xdebug.ini**`, adding the following lines:

{% highlight bash %}
xdebug.remote_enable=on
xdebug.remote_handler=dbgp
xdebug.remote_host=localhost
xdebug.remote_port=9000
{% endhighlight %}

Xdebug should now be working - running `php -m` on its own should list xdebug as one of the loaded extensions.


## 2. Configure Netbeans IDE


Go to **Tools -> Options**, then click the **PHP** tab. Under "Debugging", the following options should be set:



	
  * Debugger port: 9000

	
  * Session ID: netbeans-xdebug

	
  * Stop at first line: (ticked)


This is all that's needed to set up Netbeans in general, but it's slightly more complex configuring each project.


## 3. Configure Netbeans project


There's a huge variety of ways that you can set up a web project on your LAMP configuration, so I'll try to make it clear what each option means. To edit the settings for an individual project, right click on the project in the Projects side bar, and choose _Properties_.

This panel has several categories on the right, and should have _Sources_ selected. On this panel, set the _Web Root_ to the directory containing your **index.php** file (for CakePHP projects this should be **app/webroot **- this is essential for making it work).

Next, select the _Run Configuration_ panel. This is where you set up profiles for running your web application. You can have multiple profiles for different configurations, which can come in handy if you are using a framework that also supports command line execution. Choose "Local Web SIte (running on local web server)" as the _Run As_ option, and then enter the local URL of your project (this could be _http://localhost_, or whatever host name you've given). The panel should now look like this:

[![Project properties panel showing Run Configuration](http://blog.joncairns.com/wp-content/uploads/2012/01/Screenshot-Project-Properties-300x212.png)](http://blog.joncairns.com/wp-content/uploads/2012/01/Screenshot-Project-Properties.png)<span class="caption">Run Configuration panel in project properties</span>

_Note: many people have one local website running at a time (i.e. http://localhost), and swap the Apache document root as and when they need. I prefer to set my sites up as virtual hosts, and give them each internal host names (e.g. http://joncairns.local)._

Now, on the same panel, click "Advanced", which will open up a new window. The most important thing you should do here is add "localhost" as the debugger proxy at the bottom of the window. Another thing that you would probably like to do is choose "Ask Every Time"  for the debug URL, which will ask you to specify the URL when running the debugger, otherwise it will always go to the root URL.

You should now be ready to run the debugger. Click on a line number somewhere in your code (a line that you know will be executed) to mark it as a breakpoint; this means that the execution will pause when it gets to this line. This is what a breakpoint looks like:

![Shows a breakpoint in Netbeans IDE](http://blog.joncairns.com/wp-content/uploads/2012/01/Breakpoint.png)<span class="caption">Click on the line number to activate a breakpoint</span>

One of the buttons at the top of the Netbeans window, below the menu, is the button to start the debugger (tooltip "Debug Main Project") - click it. Your browser should open a tab with your project URL, and hang. You will then see that the breakpoint that you set has changed colour, meaning that the debugger has reached that point. You can use the debug windows to browse any variables in the current scope, which is totally awesome! I'll leave it to you to use its full potential. When you're ready to move on, click the icon that looks like a white arrow in a green circle, at the top of the Netbeans window. This will tell the debugger to move to the next breakpoint, or complete the execution if there aren't any more set. You can stop the debugger at any point by clicking the big red square icon.


## 4. Integrating with Chrome/Firefox (optional)


Currently we can kick off a debug session from Netbeans, which opens a browser window. But what about if we want to go the other way, starting with our browser, and wake the debugger by reloading our website? This is possible for Google Chrome or Firefox, using plugins.


### Google CHrome


This is my personal choice, and there's a neat plugin called [Xdebug helper](https://chrome.google.com/webstore/detail/eadndfjplgieldjbigjakmdgkmoaaaoc) that does a good job of linking your sites with Netbeans and Xdebug. Download and install, then go to **Tools -> Extensions **and open the options for Xdebug helper. Under **Domains**, enter the domain of your site (e.g. _localhost _or _joncairns.local_). Then open up a tab with your site and click the little bug icon in the far right of the URL bar until he turns green. Start up the debugger in Netbeans, and leave it running. Now, every time you load your site it will use Xdebug.


### Firefox


To be perfectly honest I haven't tried it in Firefox, but I know that an extension exists - try [Easy Xdebug](https://addons.mozilla.org/en-US/firefox/addon/easy-xdebug/). If anyone fancies giving a step-by-step guide, then that would be welcome.


## 5. Command-line debugging


To start a debugging session from a command line script you need to set an environment variable before the PHP executable starts. I find that the easiest way to do this is to create a bash script that starts PHP with this variable set, and put it in _/usr/bin/php-xdebug_:

{% highlight bash %}
#!/bin/bash
export XDEBUG_CONFIG="idekey=netbeans-xdebug"
/usr/bin/php "$@"
{% endhighlight %}

Run `chmod +x` on the new file and run the PHP script with:

{% highlight bash %}
php-xdebug myscript.php
{% endhighlight %}

Happy debugging!
