---
author: joonty
comments: true
date: 2012-03-13 13:52:19+00:00
layout: post
slug: make-your-netbeans-amazing-with-jvi
title: Make your Netbeans amazing with jVi
wordpress_id: 435
categories:
- Vim
tags:
- netbeans
---

I've been a Vim user for a couple of years, and love it. After a steep learning curve, your coding speed increases dramatically. As good as IDEs like Netbeans and Eclipse can be, the normal operations - navigating round the file, editing, copying, pasting, deleting - are never as efficient as they are when using Vim.

If you don't agree with me about vim, or you don't know what I'm talking about, read this article: [Why, oh WHY, do those #?@! nutheads use vi?](http://www.viemu.com/a-why-vi-vim.html)

Two worlds are about to collide: there's a fantastic plugin for Netbeans called jVi ([http://jvi.sourceforge.net/](http://jvi.sourceforge.net/)), which allows you to work with all the features of Netbeans and edit code as if you were using Vim. You can install it from _Tools -> Plugin_ in your Netbeans. It works beautifully, and any Vim user will immediately feel at home. In fact, I'm forgetting what it's like to live without it.

If you give it the time it deserves, I promise you that you won't want to go back to the way things used to be!
