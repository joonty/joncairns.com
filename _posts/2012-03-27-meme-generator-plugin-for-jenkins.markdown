---
author: joonty
comments: true
date: 2012-03-27 12:38:33+00:00
layout: post
slug: meme-generator-plugin-for-jenkins
title: Meme Generator plugin for Jenkins
wordpress_id: 438
categories:
- Projects
tags:
- ci
- java
- jenkins
- plugin
---

Over the past few months I've been secretly working on a [Jenkins](http://jenkins-ci.org/) plugin, partly because of my love for the chipper butler and all his efforts to improve the quality of your code, and partly because I wanted to take a look at Java.

The plugin is a meme generator. Don't know what a meme is? Take a look at [memegenerator.net](http://memegenerator.net), and any one of those pictures is a meme. The plugin creates a new meme if your project's build fails, using the memegenerator.net API, and posts it on the project page. Who broke the build? No more hiding - they're immortalised in an image. Just a bit of fun.

If you fancy giving it a go, sign up for a free account at memegenerator.net and install the plugin from the update center in your Jenkins instance.



	
  * **Wiki page: **[https://wiki.jenkins-ci.org/display/JENKINS/Meme+Generator+Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Meme+Generator+Plugin)

	
  * **Source code: **[https://github.com/jenkinsci/memegen-plugin](https://github.com/jenkinsci/memegen-plugin/)


