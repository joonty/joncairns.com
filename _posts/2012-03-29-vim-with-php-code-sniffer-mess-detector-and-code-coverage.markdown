---
author: joonty
comments: true
date: 2012-03-29 13:29:29+00:00
layout: post
slug: vim-with-php-code-sniffer-mess-detector-and-code-coverage
title: Vim with PHP code sniffer, mess detector and code coverage
wordpress_id: 443
categories:
- Vim
tags:
- phpqa
---

Vim is a lot more powerful than people think. Many people who use it would do so just for quickly editing a single file, but would use an IDE for their main development. But I've discovered recently that Vim is so extensible that it can do all the things that you thought it couldn't: code completion, syntax checking, project management, debugger integration and more.

Check out [http://vim.wikia.com/wiki/Use_Vim_like_an_IDE](http://vim.wikia.com/wiki/Use_Vim_like_an_IDE) and [http://arstechnica.com/open-source/guides/2009/05/vim-made-easy-how-to-get-your-favorite-ide-features-in-vim.ars](http://arstechnica.com/open-source/guides/2009/05/vim-made-easy-how-to-get-your-favorite-ide-features-in-vim.ars) for more on that.

I now use Vim as my main PHP editor, and one thing that I've managed to get working better than any other IDE I've used is integration with quality assurance tools like [PHP code sniffer](http://pear.php.net/package/PHP_CodeSniffer/redirected), [PHP mess detector](http://phpmd.org/) and clover code coverage (produced from [PHPUnit](https://github.com/sebastianbergmann/phpunit/) tests). I've written [phpqa](https://github.com/joonty/vim-phpqa) (vim script [#3980](http://www.vim.org/scripts/script.php?script_id=3980)), a plugin for Vim that integrates these three tools with your code by highlighting and marking lines with violations and code coverage output. Here are some screenshots showing it at work:

[![PHP syntax error marker](http://blog.joncairns.com/wp-content/uploads/2012/03/vim-phpqa2-1024x568.png)](http://blog.joncairns.com/wp-content/uploads/2012/03/vim-phpqa2.png)<span class="caption">Showing PHP syntax error caused by a missing semicolon</span>

[![Showing code coverage and code sniffer violations](http://blog.joncairns.com/wp-content/uploads/2012/03/vim-phpqa-1024x654.png)](http://blog.joncairns.com/wp-content/uploads/2012/03/vim-phpqa.png)<span class="caption">Showing code coverage and code sniffer violations. The lines covered by tests have a yellow "C>" marker, and lines not covered have a red marker.</span>

For more information, including installation, configuration and usage, visit the [github page](https://github.com/joonty/vim-phpqa).

Hopefully this plugin will be useful to others, and maybe even convert some people to using it as their IDE replacement! (I know, I'm a dreamer)
