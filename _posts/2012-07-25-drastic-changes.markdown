---
author: joonty
comments: true
date: 2012-07-25 13:34:36+00:00
layout: post
slug: drastic-changes
title: Drastic changes
wordpress_id: 532
categories:
- General
post_format:
- Status
tags:
- update
---

As you may have noticed, some drastic reshuffling has occurred on the site. I've decided that it was futile to try to keep several totally different categories of blog running at the same time, so I've turned this blog into a purely technical one. Hopefully that will make it easier to navigate, and will be more useful to more people!
