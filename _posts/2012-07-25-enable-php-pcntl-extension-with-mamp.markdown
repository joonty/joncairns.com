---
author: joonty
comments: true
date: 2012-07-25 13:26:11+00:00
layout: post
slug: enable-php-pcntl-extension-with-mamp
title: Enable PHP PCNTL extension with MAMP
wordpress_id: 528
categories:
- Dev
tags:
- mac
- pcntl
---

PHP as part of MAMP (Mac, Apache, MySQL and PHP) does not come with the process control extension enabled. There is a way of compiling PCNTL as an extension and linking it in to an existing PHP build, but it's a bit in-depth.

I'm doing the following on Mac OSX Snow Leopard (64bit), with MAMP and PHP version 5.3.6. Remember to change PHP version numbers in the following lines if yours is different!

_Please note that `make` is required, which isn't installed by default on Mac OSX. You need to install this via Mac developer tools, [http://developer.apple.com/unix/](http://developer.apple.com/unix/)_

First, download a tar of the PHP source code that matches the version you are using in MAMP (e.g. mine is 5.3.6), which you can do at [http://www.php.net/releases/](http://www.php.net/releases/). Untar and CD to _php-[version]/ext/pcntl_, e.g.:

{% highlight bash %}
$ wget http://museum.php.net/php5/php-5.3.6.tar.gz
$ tar xvf php-5.3.6.tar.gz
$ cd php-5.3.6/ext/pcntl
{% endhighlight %}

You then need to run `phpize` in the pcntl directory, which is a binary file that comes with MAMP:

{% highlight bash %}
pcntl$ /Applications/MAMP/bin/php/php5.3.6/bin/phpize
{% endhighlight %}

This creates a bunch of files that are needed for preparing a extension for compiling.

We now need to add some flags to tell it to compile the library with dual 32bit and 64bit architecture, as the MAMP PHP has been built this way. If you don't do this, the compiled shared objects won't work.

{% highlight bash %}
pcntl$ MACOSX_DEPLOYMENT_TARGET=10.6
pcntl$ CFLAGS="-arch i386 -arch x86_64 -g -Os -pipe -no-cpp-precomp"
pcntl$ CCFLAGS="-arch i386 -arch x86_64 -g -Os -pipe"
pcntl$ CXXFLAGS="-arch i386 -arch x86_64 -g -Os -pipe"
pcntl$ LDFLAGS="-arch i386 -arch x86_64 -bind_at_load"
pcntl$ export CFLAGS CXXFLAGS LDFLAGS CCFLAGS MACOSX_DEPLOYMENT_TARGET
{% endhighlight %}

We can then run `./configure` and `make` to build our shared object:

{% highlight bash %}
pcntl$ ./configure
pcntl$ make
{% endhighlight %}

This puts a file called `pcntl.so` in the _modules_ directory. Copy this file to your MAMP's PHP extensions directory:

{% highlight bash %}
pcntl$ cp modules/pcntl.so /Applications/MAMP/bin/php/php5.3.6/lib/php/extensions/no-debug-non-zts-20090626/
{% endhighlight %}

Finally, edit the PHP INI file to include the extension:

{% highlight bash %}
$ echo "extension=pcntl.so" >> /Applications/MAMP/bin/php/php5.3.6/conf/php.ini
{% endhighlight %}

PCNTL should now be enabled. To check to see whether it has been added, just run:

{% highlight bash %}
$ /Applications/MAMP/bin/php/php5.3.6/bin/php --ri pcntl

pcntl

pcntl support => enabled
{% endhighlight %}

If you see that, it's worked! If anything has gone wrong you can just remove the `pcntl.so` file from the MAMP PHP extensions directory and remove the INI setting, and try again.
