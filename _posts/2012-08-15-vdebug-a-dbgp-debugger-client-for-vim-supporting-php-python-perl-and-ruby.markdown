---
author: joonty
comments: true
date: 2012-08-15 13:01:05+00:00
layout: post
slug: vdebug-a-dbgp-debugger-client-for-vim-supporting-php-python-perl-and-ruby
title: 'Vdebug: a DBGP debugger client for Vim supporting PHP, Python, Perl and Ruby'
wordpress_id: 541
categories:
- Projects
tags:
- perl
- python
- ruby
- vdebug
- xdebug
---

_[To download Vdebug, go to https://github.com/joonty/vdebug](https://github.com/joonty/vdebug)_


Until recently, for debugging my programs in Vim I've been using my [own fork](https://github.com/joonty/vim-xdebug) of the PHP & Xdebug Vim plugin originally created by Seung Woo Shin. This worked well, although it sometimes ran slowly and ended up breaking under certain, apparently random, circumstances. On top of that, I'd done some extending to include new features, but the source code was getting too convoluted and difficult to maintain.

I also knew that there were other languages that had debugger engines using the DBG protocol, but this script was specifically aimed at PHP and Xdebug, and it wouldn't work with these other languages and engines.


## D.I.Y.


I knew that it would mean a total rewrite to fix these issues, but I was feeling hardy that day. Over the course of the next couple of months I created Vdebug, which is an attempt to bring all your debugging needs under one roof.

Like the old script, it's written largely in Python, but I made more of an attempt to modularise it. Therefore, there's a package-like structure to the code. I also wanted to write unit tests, which is something I've never done before with Python, so there are some of these tests covering the more critical parts of the code.

I'm pretty pleased with what I came up with in the end: it's much more of a full package than any of the other Vim plugins I've written, as it contains a very extensive Vim help file that covers every part of the plugin.

If you want to try it out, go to the Github repository mentioned at the top of the page and read the README on the front page to get installation instructions. After installing, run

{% highlight vim %}
:help Vdebug
{% endhighlight %}

to get help on how to use it.


## There's life outside of PHP


As I said, I wanted to add support for other languages and their debuggers. ActiveState (the makers of Komodo Edit/IDE) helpfully provide debugger engine scripts for Python, Perl and Ruby, which can be used in conjunction with Vdebug to debug your own scripts (see `:help VdebugSetUp` for instructions on setting up each language's debugger engine).


## See how it works


Part of the modularisation of the code means that it's possible to run some of Vdebug from the command line. I did this at the early stage to make development easier - I didn't need the Vim GUI to test the connection and data transfer between the client and engine, and it was easier to test separating the two.

I don't know how useful this is, but for people who like to do random stuff for the sake of it, try something like this (works on Linux):

{% highlight bash %}
$ cd ~/.vim/bundle/vdebug/plugin/python
$ python
Python 2.7.3 (default, Apr 20 2012, 22:39:59)
[GCC 4.6.3] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import vdebug.dbgp
>>> conn = vdebug.dbgp.Connection()
>>> api = vdebug.dbgp.Api(conn)
Waiting for a connection (this message will self-destruct in  30  seconds...)
{% endhighlight %}

Now, start up your script with the debugger engine active (e.g. set the IDE key/GET variable that you normally use) to create the connection. The prompt will return, and you can now access all the methods on the `vdebug.dbgp.Api` class:

{% highlight python %}
>>> response = api.step_into()
>>> print response.as_string()
<?xml version="1.0" encoding="iso-8859-1"?>
<response xmlns="urn:debugger_protocol_v1" xmlns:xdebug="http://xdebug.org/dbgp/xdebug" command="step_into" transaction_id="2" status="break" reason="ok"><xdebug:message filename="file:///usr/local/bin/cake" lineno="4"></xdebug:message></response>
>>> print api.feature_get('max_depth')
1
>>>
{% endhighlight %}

Etcetera. Happy debugging!
