---
author: joonty
comments: true
date: 2012-08-18 13:28:35+00:00
layout: post
slug: new-hosting
title: New hosting
wordpress_id: 544
categories:
- General
post_format:
- Status
---

Yesterday, I changed the host for my website. It was only slightly rocky, involving a couple of hours of down time, but all in all pretty good. And I'm very pleased to be free of my previous host, _they who shall not be named_. Let's just say that they're one of the biggest web hosts. I'm now with [Mythic Beasts](http://mythic-beasts.com), who have been very friendly and helpful and offer great deals on all sorts of hosting - check them out.
