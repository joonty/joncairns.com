---
author: joonty
comments: true
date: 2012-09-21 13:04:44+00:00
layout: post
slug: compiling-vim-with-ruby-from-rvm-on-ubuntu
title: Compiling Vim with Ruby from RVM (on Ubuntu)
wordpress_id: 548
categories:
- Vim
tags:
- linux
- ruby
- rvm
- ubuntu
---

The other day, I was in the position where I had to recompile Vim (and GVim) on Ubuntu. I had installed the version from aptitude, which is perfectly good apart from the fact that it requires and installs the ruby 1.8.7 package. I actually use [RVM](https://rvm.io) for my ruby binary management, and I wanted to tell Vim to use the ruby 1.9.3 installed on my system via RVM.

This meant recompiling, which is not too tricky. Here's how I did it.


### 1. Remove existing versions


This will depend on the OS, but for Ubuntu you can do:

{% highlight bash %}
sudo apt-get purge vim vim-gnome vim-gtk vim-tiny vim-scripts
{% endhighlight %}

And whichever vim related packages you might have lurking on your machine.

If you didn't install Vim through a package manager (i.e. compiled it), then you can run `make uninstall` in the source code directory that you used to compile. If that no longer exists, it's a case of removing the binaries manually, or hoping for the best.


### 2. Get source code


I followed the instructions at [http://www.vim.org/mercurial.php](http://www.vim.org/mercurial.php), to get the latest code base using Mercurial. If you don't have Mercurial and you are using Ubuntu, it's a simple case of `apt-get install mercurial`.

Then, to download the sources:

{% highlight bash %}
hg clone https://vim.googlecode.com/hg/ vim
hg pull
hg update
{% endhighlight %}

The last two commands are probably not necessary, as the clone should give you the updates.


### 3. Install libraries for Gvim


If you want to compile Vim with support for the graphical version, Gvim, you will have to install the development versions of _libatk_, _libgtk_, _libbonoboui2_, _libcairo2_, _libx11_, _libxpm_, _libxt_ and _libgnome2_ (if using Gnome). In Ubuntu, you can just run:

{% highlight bash %}
sudo apt-get install libncurses-dev libgnome2-dev \
   libgtk2.0-dev libatk1.0-dev libbonoboui2-dev libcairo2-dev \
   libx11-dev libxpm-dev libxt-dev
{% endhighlight %}

This will be different if you're using a different distro or package manager.



### 4. Configure for your environment


Now to configure Vim with the options you want. I wanted all the major features, plus ruby and python support. I also wanted to install GVim, and to integrate it with gnome. In the vim source directory, run:

{% highlight bash %}
./configure --help
{% endhighlight %}

To get all the options. For my setup, I used:

{% highlight bash %}
./configure --with-features=HUGE \
    --enable-pythoninterp=yes \
    --with-python-config-dir=/usr/lib/python2.7/config/ \
    --enable-multibyte=yes \
    --enable-cscope=yes \
    --enable-rubyinterp=yes \
    --with-ruby-command=/home/jon/.rvm/rubies/ruby-1.9.3-p194/bin/ruby \
    --enable-gui=gnome2 \
    --with-x \
    --enable-fontset
{% endhighlight %}

The things to change are the python config directory and ruby command. The ruby command is whichever ruby binary you want to use, and specifying a different version here will be fine. Also, if you aren't using gnome window manager then you will want to change the `--enable-gui` value to something else like "gtk2" (see `./configure --help` for all the options).

The other thing to mention is that it's including all major features with the "HUGE" value. This can be tiny, small, normal, big or huge, depending on what level you want.

Modify the command to fit your requirements and run. One reason for failure that I have had is that the ncurses library is missing. On Ubuntu you can install this with `apt-get install libcurses-dev`.

Only after `configure` has passed can you move on to the next step.


### 5. Install


Finally, after the configuration has successfully completed, you can run `make`:

{% highlight bash %}
make
# ...
# lots of output
# ...
sudo make install
{% endhighlight %}

If that works without any errors then congratulations, your Vim is ready! There are several problems that you could run into. For instance, you might get an error like:

{% highlight bash %}
if_python.c:47:20: fatal error: Python.h: No such file or directory
compilation terminated.
make[1]: *** [objects/if_python.o] Error 1
make[1]: Leaving directory `/home/jon/source/vim/src'
make: *** [first] Error 2
{% endhighlight %}

This means that you need the `python-dev` package, which you can install on Ubuntu with `apt-get install python-dev`.


### 6. Run and Enjoy


Check that your installation is working by running Vim and, in the command window, typing:

{% highlight vim %}
:ruby puts "hello, Vim!"
{% endhighlight %} 
