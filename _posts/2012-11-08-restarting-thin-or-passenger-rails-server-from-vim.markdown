---
author: joonty
comments: true
date: 2012-11-08 11:36:32+00:00
layout: post
slug: restarting-thin-or-passenger-rails-server-from-vim
title: Restarting thin or passenger Rails server from Vim
wordpress_id: 566
categories:
- Vim
tags:
- passenger
- rails
- ruby
- thin
---

I've set up Nginx with Phusion Passenger to run my Rails apps in a development environment, as opposed to running `rails server` in a terminal every time. Setting the rails environment variable to "development" means that many code changes can be automatically integrated without a server restart, but every so often a full restart is required.

This involves touching the _tmp/restart.txt _file in the rails app directory. As I spend all my time working in Vim, I created a handy function to do it for me:

{% highlight vim %}
command! RestartRails call RestartRails(getcwd())

function! RestartRails(dir)
    let l:ret=system("touch ".a:dir."/tmp/restart.txt")
    if l:ret == ""
        echo "Restarting Rails, like a boss"
    else
        echohl Error | echo "Failed to restart rails - is your working directory a rails app?" | echohl None
    endif
endfunction
{% endhighlight %}

All you do is run `:RestartRails`, and it touches the restart file. It assumes that your rails app directory is the current working directory in vim (`:pwd` gives you that, and `:cd ...` changes it). If you want to restart a different rails app, run

{% highlight vim %}
call RestartRails('/path/to/app')
{% endhighlight %} 
