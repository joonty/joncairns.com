---
author: joonty
comments: true
date: 2013-06-11 16:12:27+00:00
layout: post
slug: copying-between-tmux-buffers-and-the-system-clipboard
title: Copying between tmux buffers and the system clipboard
wordpress_id: 610
categories:
- Dev
tags:
- tmux
---

One of the initially confusing things about tmux is that it has it's own set of buffers for copying and pasting data. Copying in tmux is easiest if you're using vi mode:

{% highlight bash %}
setw -g mode-keys vi
bind-key -t vi-copy 'v' begin-selection
bind-key -t vi-copy 'y' copy-selection
{% endhighlight %}

Then you can copy with the following operations:



	
  1. **<Prefix>-[     **go into copy mode

	
  2. **v                    **go into visual select mode, select some text with the arrow keys

	
  3. **y                    **copy the highlighted text


That takes you out of copy mode, and you have the copied text in your first tmux buffer. You can paste the buffer with `<Prefix>-]`. The top buffer is always the most recently selected text.

You can see all of your tmux buffers with `<Prefix>-#`, or the shell command `tmux list-buffers`. You can also select and paste any of these buffers with `<Prefix>-=`.


## Working with the system clipboard


This is fine, until you want to copy something in the terminal to then paste in your browser or editor. Tmux's buffers are totally independent of your system's clipboard, so copying in one doesn't affect the other.

I've added a shortcut that works on systems that use xclip for the system clipboard (e.g. Debian). It copies the top tmux buffer to xclip's clipboard, and is bound to a keyboard shortcut:

{% highlight bash %}
bind y run-shell "tmux show-buffer | xclip -sel clip -i" \; display-message "Copied tmux buffer to system clipboard"
{% endhighlight %}

If you add this to your _tmux.conf_ file then you can use the shortcut `<Prefix>-y` to copy whatever is in your top tmux buffer to your system clipboard.
