---
author: joonty
comments: true
date: 2014-04-04 16:51:31+01:00
layout: post
slug: major-updates
title: Major updates
categories:
- General
---

Just a quick one to say that I've finally left Wordpress, and replaced it with a completely static site using [Jekyll](http://jekyllrb.com). I've also integrated [disqus](http://disqus.com) for my comments, which allowed me to make the transition, due to it being JavaScript based.

I also gave it a totally new design, one which I hope is a lot cleaner and unobtrusive. It's funny how a design that you like gradually becomes more and more annoying as time goes on.

Since the site is now static, it loads *much* quicker. I checked my Google Analytics stats, and found that my Wordpress blog had an average page load time of *9.99 seconds*. I'm amazed that you all put up with that! Well, it should now be a lot better for everyone.

It's been a pretty painless transition, and I have absolutely no regrets. There were plenty of blog posts about moving from Wordpress to Jekyll, so I'd recommend it to anyone who's in a similar position.

For me, the worst thing about using Wordpress was the page load time in the admin area - the dashboard would regularly time out, so I had to hack the URL to skip to the posts. And even then, we're talking 20 seconds to load the posts index. If I'm honest it put me off writing posts, as I didn't want to deal with the pain. Hopefully using Jekyll will encourage me to write more posts.

Here's to the future!
