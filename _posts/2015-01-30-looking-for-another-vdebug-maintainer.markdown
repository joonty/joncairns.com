---
author: joonty
comments: true
date: 2015-01-30
layout: post
slug: looking-for-another-vdebug-maintainer
title: Looking for another vdebug maintainer
categories:
- General
tags:
- vdebug
- python
- vim
---

Back in 2011 I created the [Vdebug](https://github.com/joonty/vdebug) plugin for Vim, which provides a visual debugger interface for debuggers that support the DBGP protocol, the main one being Xdebug for PHP. I created it mostly for myself, as I wasn't happy with the other plugins out there. At the time I was almost entirely developing in PHP, but Python was the first language that I really enjoyed, so Vdebug was an opportunity to return to it.

I had no idea that it would become so popular; as it stands currently, 399 people have starred the repo on Github and there are 63 forks. I don't think a week has gone by where there hasn't been any activity in the issues section. Github tells me that there have been 268 unique clones in the last two weeks. That's exciting, but with that comes a certain level of responsibility. This was the first project that I created that had any real traction, so I was learning how to manage open-source projects as I went. Basic lessons, like "don't push code that breaks stuff", were learnt early on.

I'm absolutely thrilled that so many people have enjoyed Vdebug. I've had strangers email me out of the blue to tell me how much the project has helped them, and how thankful they are for it, which is amazing. People who create issues on Github are almost always incredibly friendly and happy to help where they can.

The problem for me is this: 2 years ago I all but stopped developing in PHP, in favour of Ruby. About 90% of my work is in Ruby, and the other 10% split between PHP, Java, Objective-C, JavaScript and, for the purposes of Vdebug, Python. The debugging scene for Ruby is... mixed. The only DBGP compatible project (that I know of) is maintained by ActiveState for the purposes of Komodo IDE. And for some reason, whenever I download their source code I have to hack on it for a good half an hour to get it to work properly (sorry, ActiveState). It sort of works, but whenever I come across a need for debugging it's always the last thing on my list.

So, in short, I don't really use Vdebug myself all that much anymore. I'm torn between wanting to maintain this project for the apparently large number of people who use it and spending my time on projects that will help me in my current work. I have loads of ideas for Vdebug but not the time to do them justice. On top of that, I became a Dad last year, and my time has been mysteriously sucked away.

For that reason, I'm reaching out to any Vim-using Python developers (or, most likely, PHP developers with some knowledge of Python). I can manage bits of development here and there, but I'm struggling to find time to respond to issues in a timely manner (sorry to everyone who I've made wait a month, or even more, for a response :/). If there's anyone out there who's happy to roll up their sleeves and join me in maintaining this project then I'd be much more confident about it lasting, and not disappearing into insignificance.

If anyone's interested, please contact me by clicking on the mail icon in the menu, and we'll work out whether it's something you can get involved in. Thanks, and happy debugging!
