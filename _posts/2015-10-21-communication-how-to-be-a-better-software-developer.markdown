---
author: joonty
comments: true
date: 2015-10-21 15:08:33
layout: post
slug: communication-how-to-be-a-better-software-developer
title: Communication&#58; how to be a better software developer (via Medium)
image: coding.jpeg
categories:
- Dev
tags:
- people
- medium
- communication
---


Link: [Communication: how to be a better software developer](https://medium.com/@joonty/communication-how-to-be-a-better-software-developer-869c50767701#.2oek8yicq)

I haven't written a Medium post for a while, but felt like Medium was a better place for this post to appear. It presents my ideas on how social skills are essential in software development. Please read it, and share if you enjoy!

