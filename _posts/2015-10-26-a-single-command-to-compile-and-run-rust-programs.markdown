---
author: joonty
comments: true
date: 2015-10-26 14:00:30
layout: post
slug: a-single-command-to-compile-and-run-rust-programs
title: A single command to compile and run Rust programs
image: rust.png
categories:
- Dev
tags:
- rust
- bash
---

> TL;DR
>
> Compile and run rust programs with one simple bash script, i.e. `rust program.rs`.

I've been working my way through [rust by example](http://rustbyexample.com/) recently, and found myself creating and running a lot of small rust programs. They're all going in the same folder, as it's not worth creating a new cargo project for each program in the tutorial. One thing that I'm missing from interpreted languages is a simple run command, i.e. a command that will compile and run the program immediately.

I found [this issue](https://github.com/rust-lang/rust/issues/9826) in the GitHub repository, where this very thing is discussed. Apparently there used to be a `rust` command, as opposed to the `rustc` command that we now use for compiling, which had `run` command to serve this purpose. The issue suggests that it might make a comeback in the future.

For now, I've created a simple bash script that compiles the input `.rs` file, runs the output and then deletes the output (this stops the directory being littered with executables). I've placed this in `/usr/local/bin/rust`, and it can be run with all the same arguments as `rustc`, with the caveat that it expects the input file to be the first argument. It's extremely simple and fundamentally flawed due to the aforementioned caveat, but I thought I'd share it anyway:


{% highlight bash %}
#!/bin/bash
name=$(basename $1 .rs)
rustc $@ && ./$name && rm $name
{% endhighlight %}
