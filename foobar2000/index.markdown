---
author: joonty
comments: true
date: 2011-09-30 19:55:01+00:00
layout: default
slug: foobar2000
title: foobar2000 tutorial
wordpress_id: 236
---

## foobar2000
_foobar2000 is a DIY music player that takes some getting used to. Therefore, I created this tutorial some time ago, and have resurrected it in my new site design._

![foobar2000 audio player](http://www.foobar2000.org/button-large.png)

The tutorial is hopefully in a pretty logical order, and you will get the most out of it if you follow the sections in chronological order.


  1. [What is it?](http://joncairns.com/2010/10/1-what-is-foobar2000/)
  2. [Getting started](http://joncairns.com/2010/10/foobar2000-2-getting-started/)
  3. [Components](http://joncairns.com/2010/10/foobar2000-3-components/)
  4. [Features](http://joncairns.com/2010/10/foobar2000-4-features/)
    1. [Mass-tagging](http://joncairns.com/2010/10/foobar2000-4-1-mass-tagging/)
    2. [Playlist organisation](http://joncairns.com/2010/10/foobar2000-4-2-playlist-organisation/)
    3. [Replaygain](http://joncairns.com/2010/10/foobar2000-4-3-replaygain/)
    4. [Keyboard shortcuts](http://joncairns.com/2010/10/foobar2000-4-4-keyboard-shortcuts/)
  5. [ColumnsUI](http://joncairns.com/2010/10/foobar2000-5-columnsui/)
    1. [Finding your way around](http://joncairns.com/2010/10/foobar2000-5-1-getting-started-with-columnsui/)
    2. [Creating an iTunes style layout](http://joncairns.com/2010/10/foobar2000-5-2-creating-a-columnsui-layout/)
    3. [Tweaking](http://joncairns.com/2010/10/foobar2000-5-3-tweaking-columnsui/)
  6. [Code syntax](http://joncairns.com/2010/10/foobar2000-6-code-syntax/)
  7. [PanelsUI](http://joncairns.com/2010/10/foobar2000-7-panelsui/)
    1. [Finding your way around](http://joncairns.com/2010/10/foobar2000-7-1-finding-your-way-around-panelsui/)
    2. [PanelsUI code](http://joncairns.com/2010/10/foobar2000-7-2-panelsui-code/)
    3. [Creating a layout](http://joncairns.com/2010/10/foobar2000-7-3-creating-a-panelsui-layout/)

Please let me know if you find any broken links, mistakes or oddities. Unfortunately, I'm probably not going to be able to update it due to time constraints, but I hope it can still help people get started with the player.
