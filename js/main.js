$(function() {
    $('.social-links .email').attr('href', 'mailto:jon' + '@joncairns.com');
    var sc = $(".share-container");
    var sfx = $(".st-fixed-container");
    if (sc.length > 0) {
        var mq = window.matchMedia("(max-width: 900px)");
        if (!mq.matches) {
            $(window).scroll(function() {
                var visible = sfx.is(":visible");
                if (isScrolledIntoView(sc)) {
                    if (visible) {
                        sfx.fadeOut();
                    }
                } else if (!visible) {
                    sfx.fadeIn();
                }
            });
        }
    }
});

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = elem.offset().top;
    //var elemBottom = elemTop + elem.height();

    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
}
