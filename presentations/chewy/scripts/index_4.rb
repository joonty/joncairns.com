class ListingsIndex < Chewy::Index
  define_type Listing.all.includes(:channel) do
    #...
    field :actors, type: "object", analyzer: "keyword"

    field :start_at, :stop_at, type: "date", include_in_all: false

    field :channel_name, value: -> (listing) { listing.channel.name }
  end
end
