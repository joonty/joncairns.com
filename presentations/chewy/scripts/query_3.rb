ListingsIndex.
  filter(term: { director: "Ridley Scott" }).
  filter { year > 2000 }.
  to_a
