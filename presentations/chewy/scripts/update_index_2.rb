class Listing < ActiveRecord::Base
  update_index('listings') { self }

  belongs_to :channel
end

class Channel < ActiveRecord::Base
  update_index('listings') { listings }

  has_many :listings
end
