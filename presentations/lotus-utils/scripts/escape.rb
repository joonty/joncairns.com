require "lotus/utils/escape"

Lotus::Utils::Escape.html('<script>alert(1);</script>')
  #=> "&lt;script&gt;alert(1);&lt;&#x2F;script&gt;"

Lotus::Utils::Escape.html_attribute("some attribute")
  #=> "some&#x20;attribute"

Lotus::Utils::Escape.url("javascript:alert('xss')")
  #=> ""
