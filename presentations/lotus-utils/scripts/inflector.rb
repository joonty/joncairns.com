require "lotus/utils/inflector"

Lotus::Utils::Inflector.pluralize("cow") #=> "cows"
Lotus::Utils::Inflector.pluralize("sheep") #=> "sheep"
Lotus::Utils::Inflector.pluralize("chicken") #=> "chickina" ??
Lotus::Utils::Inflector.singularize("chickens") #=> "chicken"
