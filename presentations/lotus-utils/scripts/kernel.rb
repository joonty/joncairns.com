require "lotus/utils/kernel"

Lotus::Utils::Kernel.Array(true)        # => [true]
Lotus::Utils::Kernel.Integer("1")       # => 1
Lotus::Utils::Kernel.Time('2014-04-18') # => 2014-04-18 00:00:00 +0200
