require "lotus/utils/path_prefix"

path_prefix = Lotus::Utils::PathPrefix.new('posts')
path_prefix.join('new').to_s  # => "/posts/new"
path_prefix.join('/new').to_s # => "/posts/new"
