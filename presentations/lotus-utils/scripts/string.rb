require "lotus/utils/string"

string = Lotus::Utils::String.new 'lotus_utils'
string.classify # => "LotusUtils"
string.titleize # => "Lotus Utils"
string.dasherize # => "lotus-utils"

string = Lotus::Utils::String.new 'chicken'
string.pluralize # => "chickina"
