class ListingsIndex < Chewy::Index
  define_type Listing.all.includes(:channel) do
    field :title

    field :desc, analyzer: "snowball"

    field :has_subtitles, type: "boolean"

    field :year, type: "integer"
  end
end
