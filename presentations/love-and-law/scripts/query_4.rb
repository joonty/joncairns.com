ListingsIndex.
  filter { title =~ /gear/ }.
  filter { year? }.
  filter { (year > 2000) & (year < 2015) }.
  to_a
