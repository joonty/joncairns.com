ListingsIndex.
  filter { title =~ /gear/ }.
  order(start_at: :desc).
  limit(5).
  to_a
