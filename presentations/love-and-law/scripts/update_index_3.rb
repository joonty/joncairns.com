# Destroy and create index
ListingsIndex.purge!

# Destroy and create index, then populate
ListingsIndex.reset!

# Import a different dataset
ListingsIndex::Listing.import Listing.where(some: :condition)
