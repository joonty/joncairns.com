'use strict';

var pubnub = new PubNub({
    subscribeKey: 'sub-c-5cb758ce-09f4-11e8-b131-0a982f425d04',    // PubNub subscribe key,
    publishKey: 'pub-c-5c10c031-0aa8-4fb8-ab0d-0a865b0f32d8',        // PubNub publish key
    ssl: true
});
var hammertime;

document.onkeydown = checkKey;

function checkKey(e) {
    e = e || window.event;
    if (e.keyCode == '38') {
        // up arrow
        buttonCommand("up");
    }
    else if (e.keyCode == '40') {
        // down arrow
        buttonCommand("down");
    }
    else if (e.keyCode == '37') {
       // left arrow
        buttonCommand("left");
    }
    else if (e.keyCode == '39') {
       // right arrow
        buttonCommand("right");
    }

}

pubnub.addListener({
    message: function(event) {
        const notesEl = document.getElementById("notes"),
              contentEl = document.getElementById("content"),
              displayEl = document.getElementById("display"),
              div = document.createElement('div'),
              message = event.message;

        if (message.slide) {
            console.log("Slide change");
            contentEl.innerHTML = "";
            notesEl.innerHTML = "";

            displayEl.textContent = message.slide + '.' + message.part + "/" + message.totalSlides;
            div.innerHTML = message.content;
            var newNotesEl = div.querySelector(".notes");
            if (newNotesEl) {
                notesEl.appendChild(newNotesEl);
            }
            contentEl.appendChild(div);
        }
        if (message.fragment !== undefined) {
            console.log("Fragment change: " + message.fragment);
            var fragments = contentEl.querySelectorAll(".fragment");
            fragments.forEach(function(e, i) {
                if (i <= message.fragment) {
                    e.classList.add("shown");
                } else {
                    e.classList.remove("shown");
                }
            });
            fragments = notesEl.querySelectorAll(".nfragment");
            fragments.forEach(function(e, i) {
                if (i == message.fragment) {
                    e.classList.add("shown");
                } else {
                    e.classList.remove("shown");
                }
            });
        }
    }
});

pubnub.subscribe({
    channels: ['output']
});

function buttonCommand(button) {
    pubnub.publish({
        channel : 'input',
        message : {button: button}
    });
}

function init() {
    console.log("asking for current state");
    pubnub.publish({
        channel: 'input',
        message: "getState"
    });
}

jQuery(document).ready(function() {
    var debug = document.getElementById("debug");
    init();
    jQuery('.btn').click(function (eventObject) {
        var targetId = jQuery(this).attr('id');
        buttonCommand(targetId);
    });
    hammertime = new Hammer(document.body);
    var mc = new Hammer.Manager(document.body);
    mc.add( new Hammer.Tap({ event: 'doubletap', taps: 2, pointers: 1 }) );
    mc.add( new Hammer.Tap({ event: 'twotap', taps: 1, pointers: 2 }) );
    mc.on("doubletap", function() {
        toggleNotes($('.panel-notes .panel-heading'));
    });
    mc.on("twotap", function() {
      buttonCommand("right");
    });
    hammertime.get("pan").set({enable: true, direction: Hammer.DIRECTION_HORIZONTAL, threshold: window.innerWidth / 3});

    hammertime.on('panend', function(ev) {
        if (ev.direction == Hammer.DIRECTION_LEFT) {
            ev.preventDefault();
            buttonCommand("right");
        } else if (ev.direction == Hammer.DIRECTION_RIGHT) {
            ev.preventDefault();
            buttonCommand("left");
        }
    });

});

function toggleNotes($el) {
	if(!$el.hasClass('panel-collapsed')) {
        $el.parents('.panel').find('.panel-body').slideUp();
        $el.addClass('panel-collapsed');
        $el.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
	} else {
        $el.parents('.panel').find('.panel-body').slideDown();
        $el.removeClass('panel-collapsed');
        $el.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	}
}

$(document).on('click', '.panel-heading.clickable', function(e){
    toggleNotes($(this));
})
