<!doctype html>
%html{lang: "en"}
  %head
    %meta{charset: "utf-8"}/
    %title Rescuing Rails apps
    %meta{content: "Rescuing Rails apps", name: "description"}/
    %meta{content: "Jon Cairns", name: "author"}/
    %meta{content: "yes", name: "apple-mobile-web-app-capable"}/
    %meta{content: "black-translucent", name: "apple-mobile-web-app-status-bar-style"}/
    %meta{content: "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no", name: "viewport"}/
    %link{href: "../extra.css", rel: "stylesheet"}/
    %link{href: "../reveal/css/reveal.min.css", rel: "stylesheet"}/
    %link#theme{href: "../reveal/css/theme/night.css", rel: "stylesheet"}/
    / For syntax highlighting
    %link{href: "../reveal/lib/css/zenburn.css", rel: "stylesheet"}/
    / If the query includes 'print-pdf', use the PDF print sheet
    :javascript
      document.write( '<link rel="stylesheet" href="../reveal/css/print/' + ( window.location.search.match( /print-pdf/gi ) ? 'pdf' : 'paper' ) + '.css" type="text/css" media="print">' );
    /[if lt IE 9]
      <script src="../reveal/lib/js/html5shiv.js"></script>
  %body
    %script{src: "../reveal/lib/js/head.min.js"}
    %script{src: "../reveal/js/reveal.min.js"}

    .reveal
      .slides
        %section
          %h1{style: "text-transform: lowercase;"} Rescuing Rails apps
          %h2 When you've finally had enough
          %p
            %small
              %a{href: "http://joncairns.com"} Jon Cairns
              \/
              %a{href: "http://twitter.com/joonty"} @joonty

        %section
          %h2 Our goals
          %p We all want our code to:
          %ul
            %li.fragment be fast
            %li.fragment be beautiful
            %li.fragment work well and complete features

        %section
          %h2 The reality
          %p Projects tend towards chaos.
          %ul
            %li.fragment We rush implementing features
            %li.fragment We tack on little bits of untested code to existing features
            %li.fragment The project spec changes, leaving redundant or broken code in the codebase
            %li.fragment Our test suite gets too big and uncontrollable
            %li.fragment The project outgrows the framework, or the framework turns out to be broken

        %section
          %h2 It's demoralising
          %p Working on a problem project changes coding from something we love to something we hate.
          %img{src:"img/powerrangers.gif"}

        %section
          %h2 Decision time
          %p You have a few options:
          %ul
            %li.fragment Ignore it and carry on
            %li.fragment Throw it out and start again
            %li.fragment Refactor

        %section
          %h2 1. Ignore it and carry on
          %ul.unstyled
            %li.fragment.tick You don't need any skill for this
            %li.fragment.tick It's really easy, and takes no time at all
            %li.fragment.cross The project gets worse and worse
            %li.fragment.cross Small changes take longer and longer
            %li.fragment.cross Eventually you'll be killed
            %li.fragment.cross Your body gets eaten by possums

        %section
          %h2 Proof
          %img{src: "img/possum.jpg"}

        %section
          %h2 2. Throw it out and start again
          %ul.unstyled
            %li.fragment.tick We all like a new project
            %li.fragment.tick This time it will all be great
            %li.fragment.tick You have visibility over all the features up to now
            %li.fragment.cross It's always far more work than you think
            %li.fragment.cross Projects die because of never-ending rewrites
            %li.fragment.cross Eventually you'll be killed
            %li.fragment.cross Your body gets eaten by possums

        %section
          %h2 Proof
          %img{src: "img/possum2.jpg"}

        %section
          %h2 3. Refactor
          %ul.unstyled
            %li.fragment.tick The project can continue while you make gradual improvements
            %li.fragment.tick Time isn't wasted recreating existing features
            %li.fragment.tick It's encouraging watching a project get healthier
            %li.fragment.cross Often the most challenging approach
            %li.fragment.cross You have to get knee-deep in bad code

        %section
          %h2 How to decide
          %p There isn't one answer for each case. Ask the following:
          %ul
            %li.fragment Has the project spec changed drastically?
            %li.fragment What's the future of the project?
            %li.fragment Is the project getting regular new features?
            %li.fragment How old is the Rails version?
            %li.fragment What state is the test suite in?
            %li.fragment Are personal preferences getting in the way?

        %section
          %h2 So, you've decided to refactor?
          %p Here's a process you can use, instead of diving straight in.

        %section
          %h2 1. Static analysis
          %p Find out the state of your application in quantitive terms.
          %ol
            %li.fragment Code coverage (simplecov)
            %li.fragment Code quality (rubycritic, includes reek, flay and flog)
            %li.fragment Rails-specific code analysis (rails_best_practices)
            %li.fragment Rails security vulnerabilties (brakeman)
            %li.fragment Code style conformity (rubocop)

        %section
          %img{src: "img/rubycritic.png"}

        %section
          %h2 2. Rank the problem areas
          %p Weigh the following up together:
          %ul
            %li.fragment Results from static analysis tools
            %li.fragment The importance of the feature
            %li.fragment The historic bugginess of the feature

        %section
          %h2 3. Determine the problem
          %ul
            %li.fragment Does the code actually do the right job or not?
            %li.fragment Are there helpful tests already in place or not?

          %p.fragment If the code AND the implementation of the feature are bad, then the section needs rewriting.

        %section
          %h2 4. Test first
          %p Let's assume three types of test:
          %ul
            %li.fragment Unit tests (low-level, high detail)
            %li.fragment Integration/function tests (mid-level, medium detail)
            %li.fragment Acceptance/E2E tests (high-level, low detail)

          %p.fragment Unit tests are the most detailed, but also the most likely to be thrown away in a refactor.

        %section
          %h3 If you're refactoring a feature that already works
          %p I.e. no major changes to existing functionality:
          %ol
            %li.fragment Write as many E2E tests as possible
            %li.fragment Write integration tests if possible
            %li.fragment You'll probably have to delete or heavily modify any existing unit tests
            %li.fragment Use TDD to develop new unit tests and refactored code until the E2E tests pass

        %section
          %h3 If you're rewriting a feature entirely
          %p I.e. major changes to existing functionality:
          %ol
            %li.fragment Delete existing tests covering the feature
            %li.fragment Write a couple of E2E tests to describe the new feature
            %li.fragment Use TDD to develop enough of the feature until the E2E tests pass
            %li.fragment Repeat steps 2 &amp; 3 until finished

        %section
          %h2 5. Check progress
          %p Keep running the static analysis tools on a regular basis (preferably CI), and see the stats improve!

        %section
          %h2 Time for a demo
          %img{src: "img/scared.gif"}

    :javascript
      // Full list of configuration options available here:
      // https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
          controls: true,
          progress: true,
          history: true,
          center: true,
          theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
          transition: Reveal.getQueryHash().transition || 'default', // default/cube/page/concave/zoom/linear/fade/none

          // Optional libraries used to extend on reveal.js
          dependencies: [
              { src: '../reveal/lib/js/classList.js', condition: function() { return !document.body.classList; } },
              { src: '../reveal/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
              { src: '../reveal/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
              { src: '../reveal/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
              { src: '../reveal/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
              { src: '../reveal/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }
          ]
      });
