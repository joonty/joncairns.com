<!DOCTYPE html>
<html lang="en">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Rescuing Rails apps</title>
    <meta content="Rescuing Rails apps" name="description">
    <meta content="Jon Cairns" name="author">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black-translucent" name="apple-mobile-web-app-status-bar-style">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <link href="../extra.css" rel="stylesheet">
    <link href="../reveal/css/reveal.min.css" rel="stylesheet">
    <link href="../reveal/css/theme/night.css" id="theme" rel="stylesheet">
    <!-- For syntax highlighting -->
    <link href="../reveal/lib/css/zenburn.css" rel="stylesheet">
    <!-- If the query includes 'print-pdf', use the PDF print sheet -->
    <script>
      document.write( '<link rel="stylesheet" href="../reveal/css/print/' + ( window.location.search.match( /print-pdf/gi ) ? 'pdf' : 'paper' ) + '.css" type="text/css" media="print">' );
    </script>
    <!--[if lt IE 9]>
      <script src="../reveal/lib/js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
    <script src="../reveal/lib/js/head.min.js"></script>
    <script src="../reveal/js/reveal.min.js"></script>
    <div class="reveal">
      <div class="slides">
        <section>
          <h1 style="text-transform: lowercase;">Rescuing Rails apps</h1>
          <h2>When you've finally had enough</h2>
          <p>
            <small>
              <a href="http://joncairns.com">Jon Cairns</a>
              /
              <a href="http://twitter.com/joonty">@joonty</a>
            </small>
          </p>
        </section>
        <section>
          <h2>Our goals</h2>
          <p>We all want our code to:</p>
          <ul>
            <li class="fragment">be fast</li>
            <li class="fragment">be beautiful</li>
            <li class="fragment">work well and complete features</li>
          </ul>
        </section>
        <section>
          <h2>The reality</h2>
          <p>Projects tend towards chaos.</p>
          <ul>
            <li class="fragment">We rush implementing features</li>
            <li class="fragment">We tack on little bits of untested code to existing features</li>
            <li class="fragment">The project spec changes, leaving redundant or broken code in the codebase</li>
            <li class="fragment">Our test suite gets too big and uncontrollable</li>
            <li class="fragment">The project outgrows the framework, or the framework turns out to be broken</li>
          </ul>
        </section>
        <section>
          <h2>It's demoralising</h2>
          <p>Working on a problem project changes coding from something we love to something we hate.</p>
          <img src="img/powerrangers.gif">
        </section>
        <section>
          <h2>Decision time</h2>
          <p>You have a few options:</p>
          <ul>
            <li class="fragment">Ignore it and carry on</li>
            <li class="fragment">Throw it out and start again</li>
            <li class="fragment">Refactor</li>
          </ul>
        </section>
        <section>
          <h2>1. Ignore it and carry on</h2>
          <ul class="unstyled">
            <li class="fragment tick">You don't need any skill for this</li>
            <li class="fragment tick">It's really easy, and takes no time at all</li>
            <li class="fragment cross">The project gets worse and worse</li>
            <li class="fragment cross">Small changes take longer and longer</li>
            <li class="fragment cross">Eventually you'll be killed</li>
            <li class="fragment cross">Your body gets eaten by possums</li>
          </ul>
        </section>
        <section>
          <h2>Proof</h2>
          <img src="img/possum.jpg">
        </section>
        <section>
          <h2>2. Throw it out and start again</h2>
          <ul class="unstyled">
            <li class="fragment tick">We all like a new project</li>
            <li class="fragment tick">This time it will all be great</li>
            <li class="fragment tick">You have visibility over all the features up to now</li>
            <li class="fragment cross">It's always far more work than you think</li>
            <li class="fragment cross">Projects die because of never-ending rewrites</li>
            <li class="fragment cross">Eventually you'll be killed</li>
            <li class="fragment cross">Your body gets eaten by possums</li>
          </ul>
        </section>
        <section>
          <h2>Proof</h2>
          <img src="img/possum2.jpg">
        </section>
        <section>
          <h2>3. Refactor</h2>
          <ul class="unstyled">
            <li class="fragment tick">The project can continue while you make gradual improvements</li>
            <li class="fragment tick">Time isn't wasted recreating existing features</li>
            <li class="fragment tick">It's encouraging watching a project get healthier</li>
            <li class="fragment cross">Often the most challenging approach</li>
            <li class="fragment cross">You have to get knee-deep in bad code</li>
          </ul>
        </section>
        <section>
          <h2>How to decide</h2>
          <p>There isn't one answer for each case. Ask the following:</p>
          <ul>
            <li class="fragment">Has the project spec changed drastically?</li>
            <li class="fragment">What's the future of the project?</li>
            <li class="fragment">Is the project getting regular new features?</li>
            <li class="fragment">How old is the Rails version?</li>
            <li class="fragment">What state is the test suite in?</li>
            <li class="fragment">Are personal preferences getting in the way?</li>
          </ul>
        </section>
        <section>
          <h2>So, you've decided to refactor?</h2>
          <p>Here's a process you can use, instead of diving straight in.</p>
        </section>
        <section>
          <h2>1. Static analysis</h2>
          <p>Find out the state of your application in quantitive terms.</p>
          <ol>
            <li class="fragment">Code coverage (simplecov)</li>
            <li class="fragment">Code quality (rubycritic, includes reek, flay and flog)</li>
            <li class="fragment">Rails-specific code analysis (rails_best_practices)</li>
            <li class="fragment">Rails security vulnerabilties (brakeman)</li>
            <li class="fragment">Code style conformity (rubocop)</li>
          </ol>
        </section>
        <section>
          <img src="img/rubycritic.png">
        </section>
        <section>
          <h2>2. Rank the problem areas</h2>
          <p>Weigh the following up together:</p>
          <ul>
            <li class="fragment">Results from static analysis tools</li>
            <li class="fragment">The importance of the feature</li>
            <li class="fragment">The historic bugginess of the feature</li>
          </ul>
        </section>
        <section>
          <h2>3. Determine the problem</h2>
          <ul>
            <li class="fragment">Does the code actually do the right job or not?</li>
            <li class="fragment">Are there helpful tests already in place or not?</li>
          </ul>
          <p class="fragment">If the code AND the implementation of the feature are bad, then the section needs rewriting.</p>
        </section>
        <section>
          <h2>4. Test first</h2>
          <p>Let's assume three types of test:</p>
          <ul>
            <li class="fragment">Unit tests (low-level, high detail)</li>
            <li class="fragment">Integration/function tests (mid-level, medium detail)</li>
            <li class="fragment">Acceptance/E2E tests (high-level, low detail)</li>
          </ul>
          <p class="fragment">Unit tests are the most detailed, but also the most likely to be thrown away in a refactor.</p>
        </section>
        <section>
          <h3>If you're refactoring a feature that already works</h3>
          <p>I.e. no major changes to existing functionality:</p>
          <ol>
            <li class="fragment">Write as many E2E tests as possible</li>
            <li class="fragment">Write integration tests if possible</li>
            <li class="fragment">You'll probably have to delete or heavily modify any existing unit tests</li>
            <li class="fragment">Use TDD to develop new unit tests and refactored code until the E2E tests pass</li>
          </ol>
        </section>
        <section>
          <h3>If you're rewriting a feature entirely</h3>
          <p>I.e. major changes to existing functionality:</p>
          <ol>
            <li class="fragment">Delete existing tests covering the feature</li>
            <li class="fragment">Write a couple of E2E tests to describe the new feature</li>
            <li class="fragment">Use TDD to develop enough of the feature until the E2E tests pass</li>
            <li class="fragment">Repeat steps 2 &amp; 3 until finished</li>
          </ol>
        </section>
        <section>
          <h2>5. Check progress</h2>
          <p>Keep running the static analysis tools on a regular basis (preferably CI), and see the stats improve!</p>
        </section>
        <section>
          <h2>Time for a demo</h2>
          <img src="img/scared.gif">
        </section>
      </div>
    </div>
    <script>
      // Full list of configuration options available here:
      // https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
          controls: true,
          progress: true,
          history: true,
          center: true,
          theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
          transition: Reveal.getQueryHash().transition || 'default', // default/cube/page/concave/zoom/linear/fade/none
      
          // Optional libraries used to extend on reveal.js
          dependencies: [
              { src: '../reveal/lib/js/classList.js', condition: function() { return !document.body.classList; } },
              { src: '../reveal/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
              { src: '../reveal/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
              { src: '../reveal/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
              { src: '../reveal/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
              { src: '../reveal/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }
          ]
      });
    </script>
  </body>
</html>
