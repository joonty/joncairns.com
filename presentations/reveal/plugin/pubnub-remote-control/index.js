/*!
 * reveal.js PubNub Remote Control Plugin v. 0.1.3
 * https://github.com/salvatorecordiano/reveal-js-pubnub-remote-control-plugin/
 *
 * Copyright (C) 2016-2017 Salvatore Cordiano, http://www.salvatorecordiano.it/
 * Released under the MIT license
 */

(function() {

    'use strict';

    var PubnubRemoteControl = (function () {

        var defaultProperties = {
            subscribeKey: null,
            publishKey: null,
            inputChannel: 'input',
            outputChannel: 'output'
        };

        var thiz = function (customProperties) {

            var options = {};
            var totalSlides = Reveal.getTotalSlides();
            var indices;
            var pubnub;

            if (customProperties && typeof customProperties === 'object') {
                extendDefaultProperties(options, customProperties);
            }

            initPubNub();
            options.publishKey && initReveal();

            function initPubNub() {
                pubnub = new PubNub({ publishKey: options.publishKey, subscribeKey: options.subscribeKey, ssl: true });
                pubnub.subscribe({ channels: [ options.inputChannel ] });
                pubnub.addListener({
                    message: function(event) {
                        processInput(event.message);
                    }
                });
            }

            function initReveal() {
                sendInitialState();

                Reveal.addEventListener('slidechanged', function(event) {
                    console.log(event);
                    indices = Reveal.getIndices();
                    sendUpdate(event.indexh, event.indexv, indices.f, event.currentSlide, totalSlides);
                });
                Reveal.addEventListener( 'fragmentshown', function( event ) {
                    indices = Reveal.getIndices()
                    console.log(indices);
                    sendFragmentUpdate(indices.f);
                } );
                Reveal.addEventListener( 'fragmenthidden', function( event ) {
                    indices = Reveal.getIndices()
                    console.log(indices);
                    sendFragmentUpdate(indices.f);
                } );
            }

            function sendInitialState() {
                indices = Reveal.getIndices();
                sendUpdate(indices.h, indices.v, indices.f, Reveal.getCurrentSlide(), totalSlides);
            }

            function sendUpdate(slide, part, fragment, currentSlideEl, totalSlides)
            {
                pubnub.publish({
                    channel : options.outputChannel,
                    message : {
                        slide: (slide+1),
                        part: (part+1),
                        fragment: fragment,
                        content: currentSlideEl ? currentSlideEl.innerHTML : "",
                        totalSlides: totalSlides}
                });
            }

            function sendFragmentUpdate(fragment)
            {
                pubnub.publish({
                    channel : options.outputChannel,
                    message : {
                        fragment: fragment }

                });
            }

            function extendDefaultProperties(options, customProperties) {
                var property;
                for (var property in defaultProperties) {
                    if (defaultProperties.hasOwnProperty(property)) {
                        options[property] = defaultProperties[property];
                    }
                }
                for (property in customProperties) {
                    if (customProperties.hasOwnProperty(property)) {
                        options[property] = customProperties[property];
                    }
                }
            }

            function processInput(input) {
                if(input && typeof input === 'object' && input.button) {
                    switch (input.button) {
                        case 'left' :
                            Reveal.navigateLeft();
                            break;
                        case 'right' :
                            Reveal.navigateRight();
                            break;
                        case 'up' :
                            Reveal.navigateUp();
                            break;
                        case 'down' :
                            Reveal.navigateDown();
                            break;
                    }
                } else if (input == "getState") {
                    console.log("Sending initial state");
                    sendInitialState();
                }
            }
        };

        return thiz;

    })();

    Reveal.getConfig().pubnubRemoteControl && new PubnubRemoteControl(Reveal.getConfig().pubnubRemoteControl);

}());
